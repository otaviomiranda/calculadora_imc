import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
//import { PagesMainPage } from '../pages/pages-main/pages-main';

import { IonicStorageModule } from '@ionic/storage';

import { BrMaskerModule } from 'brmasker-ionic-3';
@NgModule({
  declarations: [
    MyApp,
    HomePage
    //PagesMainPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrMaskerModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
    //PagesMainPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
  ]
})
export class AppModule { }
