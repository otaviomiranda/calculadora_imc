import { Component } from "@angular/core";
import { NavController, AlertController, LoadingController } from "ionic-angular";

import { Storage } from "@ionic/storage";

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {

  public altura;
  public peso;

  constructor(public navCtrl: NavController,
    public storage: Storage,
    public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() { }

  calcularIMC() {

    var quadrado = (this.altura * this.altura);

    var calculo = (this.peso / quadrado);
    if (!isNaN(calculo)) {
      if (calculo < 18.5) {
        alert("Você está magro com esse indice: " + calculo);
      }
      else if (calculo >= 18.5 && calculo < 24.9) {
        alert("Você está normal com esse indice: " + calculo);
      }

      else if (calculo >= 25 && calculo < 29.9) {
        alert("Você está com sobre peso com esse indice: " + calculo);
      }
      else if (calculo >= 30 && calculo < 39.9) {
        alert("Você está com obesidade com esse indice: " + calculo);
      }
      else if (calculo > 40)
        alert("Você estácom obesidade grave com esse indice: " + calculo);
    } else {
      alert("Altura e/ou peso invalidos, digite novamente!");
    }
  }

}
